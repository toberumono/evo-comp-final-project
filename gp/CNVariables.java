package gp;

import game.Board;
import gpjpp.GPObject;
import gpjpp.GPVariables;
import gpjpp.NotLoadable;
import gpjpp.Property;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PrintStream;

public class CNVariables extends GPVariables {
	@Property public int ConnectNum = 4;
	@Property public int BoardWidth = 7;
	@Property public int BoardHeight = 6;
	@Property public int Verbosity = 0;
	@Property public String[] Players = {"Random", "Evolved"};
	
	@Property @NotLoadable public Board board;
	
	public CNVariables() {
		board = new Board(this, Players.length);
	}
	
	public CNVariables(CNVariables source) {
		super(source);
		board = new Board(this, Players.length);
	}
	
	@Override
	public byte isA() {
		return GPObject.USERVARIABLESID;
	}
	
	//get values from a stream
	@Override
	protected void load(DataInputStream is) throws ClassNotFoundException, IOException, InstantiationException, IllegalAccessException {
		super.load(is);
		ConnectNum = is.readInt();
		BoardWidth = is.readInt();
		BoardHeight = is.readInt();
		Verbosity = is.readInt();
	}
	
	//save values to a stream
	@Override
	protected void save(DataOutputStream os) throws IOException {
		super.save(os);
		os.writeInt(ConnectNum);
		os.writeInt(BoardWidth);
		os.writeInt(BoardHeight);
		os.writeInt(Verbosity);
	}
	
	//write values to a text file
	@Override
	public void printOn(PrintStream os, GPVariables cfg) {
		super.printOn(os, cfg);
		os.println("ConnectNum                = " + ConnectNum);
		os.println("BoardWidth                = " + BoardWidth);
		os.println("BoardHeight               = " + BoardHeight);
		os.println("Verbosity                 = " + Verbosity);
	}
}
