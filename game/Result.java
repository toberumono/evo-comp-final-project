package game;

public class Result {
	private final int moves, status;
	
	public Result(int moves, int status) {
		this.moves = moves;
		this.status = status;
	}
	
	public int getMoves() {
		return moves;
	}
	
	/**
	 * @return 1 if the {@link Player} won, 0 if there was a tie, and -1 if the {@link Player} lost
	 */
	public int getStatus() {
		return status;
	}
}
