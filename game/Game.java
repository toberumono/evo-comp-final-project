package game;

import gp.CNVariables;
import gpjpp.GP;
import gpjpp.GPPopulation;

import java.util.Random;

public class Game {
	public static final Random rgen = new Random();
	private final Player[] players;
	private final Board board;
	private int games;
	private final CNVariables cfg;
	private final GPPopulation pop;
	private final GP gp;
	
	//TODO change these to our population and gp classes
	//TODO probably going to need to modify this constructor to work with the GP. Maybe an alternate constructor would be appropriate?
	public Game(final CNVariables cfg, final GPPopulation pop, final GP gp) {
		this.players = new Player[cfg.Players.length];
		this.pop = pop;
		this.cfg = cfg;
		this.gp = gp;
		this.board = cfg.board;
		for (int i = 0; i < players.length; i++)
			typeSelect : switch(cfg.Players[i].toLowerCase()) {
				case "random":
					this.players[i] = Player.makePlayer(cfg.Players[i], i + 1, null);
					break typeSelect;
				//TODO add some method of pulling one random gene from the population
				//Probably going to wrap it with cfg -> {function here}
				//case "evolved":
					//this.players[i] = Player.makePlayer(cfg.Players[i], i + 1, );
			}
	}
	
	/**
	 * Plays a game using the settings in the current instance.
	 */
	public void play() {
		newGame();
		int i = 0, finalState = 0;
		//Steps through the players list and lets each player make a move.  After each move, it checks to see if the game is over and whether that player won or there was a tie.
		//If they did, it stores whether the player won or if there was a tie.
		for (; (finalState = board.isGameOver(players[i].getNumber())) == -1; i = (i + 1) % players.length) {
			if (cfg.Verbosity > 1)
				System.out.println("Player " + players[i].getNumber() + "'s turn");
			players[i].makeMove(cfg);
			if (cfg.Verbosity > 2)
				board.printBoard();
		}
		if (cfg.Verbosity > 0) {
			if (finalState == 0)
				System.out.println("Tie.  Last player: " + players[i].getNumber());
			else
				System.out.println("Player " + players[i].getNumber() + " wins.");
		}
		games++;
		for (Player player : players)
			player.gameOver(board, players[i].getNumber());
		//TODO handle fitness and other post-game stuff here
	}
	
	public void newGame() {
		board.reset();
	}
	
	public void reset() {
		newGame();
		games = 0;
	}
	
	public int getVerbosity() {
		return cfg.Verbosity;
	}
	
	public void setVerbosity(int verbosity) {
		cfg.Verbosity = verbosity;
	}
	
	public int getNumGamesPlayed() {
		return games;
	}
}