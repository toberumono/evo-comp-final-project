package game;

import gp.CNVariables;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.function.BiFunction;
import java.util.function.Function;

public class Player {
	private final String type;
	private final int number;
	private final Function<CNVariables, Integer> move;
	private final ArrayList<Result> results;
	private static final HashMap<String, BiFunction<Integer, Function<CNVariables, Integer>, Player>> constructors = new HashMap<>();
	static {
		constructors.put("random", (number, move) -> new Player("Random", number, cfg -> Game.rgen.nextInt(cfg.BoardWidth)));
		constructors.put("evolved", (number, move) -> new Player("Evolved", number, move));
	}
	
	
	/**
	 * Creates a new {@link Player} with the given player number
	 * 
	 * @param number
	 *            the id of this player, which is greater than 0
	 * @throws InvalidPlayerException
	 *             if number is less than or equal to 0
	 */
	public Player(String type, int number, Function<CNVariables, Integer> move) {
		this.type = type;
		this.move = move;
		this.number = number;
		results = new ArrayList<>();
	}
	
	public void makeMove(CNVariables cfg) {
		cfg.board.insertPiece(number, move.apply(cfg));
	}
	
	public String getType() {
		return type;
	}
	
	public int getNumber() {
		return number;
	}
	
	public void gameOver(Board board, int winner) {
		results.add(new Result(board.getNumPieces(getNumber()), winner == getNumber() ? 1 : board.isTie() ? 0 : -1));
	}
	
	public static final Player makePlayer(String type, int number, Function<CNVariables, Integer> move) {
		return constructors.get(type.toLowerCase()).apply(number, move);
	}
}