package game;

public class InvalidPlayerException extends Exception {

	public InvalidPlayerException() {
		super();
	}

	public InvalidPlayerException(String message) {
		super(message);
	}
}