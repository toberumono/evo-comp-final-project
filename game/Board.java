package game;

import gp.CNVariables;

public class Board {
	private final int[][] board;
	private final int maxPieces, width, height, pieceDisplayWidth;
	private int totalPieces;
	private final int[] pieces;
	private final int connectNum;
	
	public Board(CNVariables cfg, int numPlayers) {
		width = cfg.BoardWidth;
		height = cfg.BoardHeight;
		maxPieces = width * height;
		board = new int[width][height];
		pieces = new int[numPlayers];
		pieceDisplayWidth = pieces.length / 10 + 1;
		connectNum = cfg.ConnectNum;
	}
	
	public void reset() {
		for (int i = 0; i < width; i++)
			for (int j = 0; j < height; j++)
				board[i][j] = 0;
		for (int i = 0; i < pieces.length; i++)
			pieces[i] = 0;
		totalPieces = 0;
	}
	
	//TODO might insert some GUI hooks, but we'll see.
	/**
	 * Inserts a piece for the given <tt>player</tt> in the given <tt>column</tt> if the move is valid.
	 * 
	 * @param player
	 *            the player for which to insert the piece
	 * @param column
	 *            the column in which to attempt to insert the piece
	 * @return true if the piece was successfully inserted, otherwise false (effectively, return false if the column was
	 *         full)
	 */
	public boolean insertPiece(int player, int column) {
		int i = 0;
		for (; i < board[column].length && board[column][i] == 0; i++); //Step through the column until we either hit the bottom or a slot with a piece in it.
		if (--i < 0) //Step back one in order to get the insertion index.  If there was a piece in the top slot, return false.
			return false;
		board[column][i] = player;
		totalPieces++;
		pieces[player - 1]++;
		return true;
	}
	
	/**
	 * @param player
	 *            the player for which to get the number of pieces
	 * @return the number of pieces that have been placed by the given player
	 */
	public int getNumPieces(int player) {
		return pieces[player - 1];
	}
	
	/**
	 * Determines the maximum number of adjacent pieces in a row, column, or diagonal for each player.
	 * 
	 * @return an array of the largest number of adjacent pieces for each player. Access with
	 *         {@code getMaxAdjacentPieces()[playerNumber - 1]}
	 */
	public int[] getMaxAdjacentPieces() {
		int[] max = new int[pieces.length];
		int lastPlayer = 0, adj = 0;
		//Rows
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				if (board[j][i] != lastPlayer) {
					if (lastPlayer > 0 && adj > max[lastPlayer - 1])
						max[lastPlayer - 1] = adj;
					adj = 0;
					lastPlayer = board[j][i];
				}
				adj++;
			}
			if (lastPlayer > 0 && adj > max[lastPlayer - 1])
				max[lastPlayer - 1] = adj;
			lastPlayer = 0;
		}
		
		//Cols
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				if (board[i][j] != lastPlayer) {
					if (lastPlayer > 0 && adj > max[lastPlayer - 1])
						max[lastPlayer - 1] = adj;
					adj = 0;
					lastPlayer = board[i][j];
				}
				adj++;
			}
			if (lastPlayer > 0 && adj > max[lastPlayer - 1])
				max[lastPlayer - 1] = adj;
			lastPlayer = 0;
		}
		
		//Diagonals
		//Down + left
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				for (int k = 0; i + k < width && j + k < height; k++) {
					if (board[i + k][j + k] != lastPlayer) {
						if (lastPlayer > 0 && adj > max[lastPlayer - 1])
							max[lastPlayer - 1] = adj;
						adj = 0;
						lastPlayer = board[i + k][j + k];
					}
					adj++;
				}
				if (lastPlayer > 0 && adj > max[lastPlayer - 1])
					max[lastPlayer - 1] = adj;
				lastPlayer = 0;
			}
		}
		//Up + right
		for (int i = 0; i < width; i++) {
			for (int j = height - 1; j >= 0; j++) {
				for (int k = 0; i + k < width && j - k >= 0; k++) {
					if (board[i + k][j - k] != lastPlayer) {
						if (lastPlayer > 0 && adj > max[lastPlayer - 1])
							max[lastPlayer - 1] = adj;
						adj = 0;
						lastPlayer = board[i + k][j - k];
					}
					adj++;
				}
				if (lastPlayer > 0 && adj > max[lastPlayer - 1])
					max[lastPlayer - 1] = adj;
				lastPlayer = 0;
			}
		}
		return max;
	}
	
	//TODO do we want to return the coordinates of these pieces instead?
	/**
	 * Determines the maximum number of adjacent pieces in a row, column, or diagonal for a single player.
	 * 
	 * @param player
	 *            the player for which to determine the maximum number of adjacent pieces
	 * @return returns the highest number of adjacent pieces for the <tt>player</tt>
	 */
	//TODO update documentation
	public int getMaxAdjacentPieces(int player) {
		if (player <= 0)
			return 0;
		return getMaxAdjacentPieces()[player - 1];
	}
	
	/**
	 * Determines if the game is over after the given player's move.
	 * 
	 * @param lastPlayer
	 *            the last player to make a move in the current game.
	 * @return 1 if the player won, 0 if there was a tie, and -1 if the game isn't over
	 */
	public int isGameOver(int lastPlayer) {
		return getMaxAdjacentPieces(lastPlayer) >= connectNum ? 1 : (totalPieces == maxPieces ? 0 : -1);
	}
	
	public boolean isTie() {
		return totalPieces == maxPieces;
	}
	
	/**
	 * @return the width of the board
	 */
	public int getWidth() {
		return width;
	}
	
	/**
	 * @return the height of the board
	 */
	public int getHeight() {
		return height;
	}
	
	/**
	 * Convenience method. Calls {@link #toString()} and prints the result.
	 * 
	 * @see #toString()
	 */
	public void printBoard() {
		System.out.println(toString());
	}
	
	/**
	 * Forwards to {@link #toString(int)} with a spacing of 1
	 */
	@Override
	public String toString() {
		return toString(1);
	}
	
	/**
	 * Produces a {@link String} representation of this {@link Board}.<br>
	 * The representation is in ASCII, and uses blanks to denote unfilled spots and the Player number to denote their pieces.
	 * 
	 * @param spacing
	 *            the spacing to place around piece numbers. Must be >= 0
	 * @return a {@link String} representation of this {@link Board}
	 */
	public String toString(int spacing) {
		if (spacing < 0)
			throw new IllegalArgumentException("Cannot have a spacing of less than 0.");
		/*int capacity = (spacing * 2 + pieceDisplayWidth) * (spacing * 2 + 1); //Number of characters in a well
		capacity += (spacing * 2 + 1); //Number of characters that form the left wall of a well
		capacity += (spacing * 2 + pieceDisplayWidth + 1); //Number of characters that form the bottom of a well
		int rowWidth = (spacing * 2 + pieceDisplayWidth + 1) * width + 2; //+1 for the remaining '|' or '+' +1 for the '\n'
		StringBuilder o = new StringBuilder(capacity);*/
		String out = "";
		int numDashes = spacing + pieceDisplayWidth + spacing;
		String space = "", numberMarker = "%-" + pieceDisplayWidth + "d";
		for (int i = 0; i < spacing; i++)
			space += " ";
		String horizontalNumber = space + numberMarker + space, horizontalZero = space;
		for (int i = 0; i < pieceDisplayWidth; i++)
			horizontalZero += " ";
		horizontalZero += space;
		String dashes = "";
		for (int i = 0; i < numDashes; i++)
			dashes += "-";
		String horizontalDashed = "+", horizontalSpaced = "|";
		for (int i = 0; i < width; i++) {
			horizontalDashed += dashes + "+";
			horizontalSpaced += horizontalZero + "|"; //Horizontal zero is the correct width for a piece well already, so we'll just reuse it here
		}
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < spacing; j++)
				out += "\n" + horizontalSpaced;
			out += "\n";
			for (int j = 0; j < width; j++)
				out += "|" + (board[i][j] == 0 ? horizontalZero : String.format(horizontalNumber, board[i][j]));
			out += "|";
			for (int j = 0; j < spacing; j++)
				out += "\n" + horizontalSpaced;
			out += "\n" + horizontalDashed;
		}
		return out.substring(1); //Remove the leading newline
	}
	
	/**
	 * Produces a {@link String} representation of this board via {@link #toString(int)} with the provided <tt>spacing</tt>.
	 * 
	 * @param spacing
	 *            the spacing to place around piece numbers. Must be >= 0
	 * @param substitutions
	 *            an array of (original, replacement) pairs where even indices are originals and odd indices are
	 *            replacements. <i>These are processed as regex</i>.
	 * @return a {@link String} representation of this {@link Board}
	 */
	public String toString(int spacing, String[] substitutions) {
		String out = toString(spacing);
		for (int i = 0; i < substitutions.length; i += 2)
			out.replaceAll(substitutions[i], substitutions[i + 1]);
		return out;
	}
	
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Board))
			return false;
		Board b = (Board) o;
		if (b.width != width || b.height != height || b.pieces.length != pieces.length)
			return false;
		for (int i = 0; i < width; i++)
			for (int j = 0; j < height; j++)
				if (b.board[i][j] != board[i][j])
					return false;
		return true;
	}
	
	public int[] calcFitness(int lastPlayer) {
		int selfFit;
		int otherFit;
		
		// fitness is minimized, where 1 is the best possible fitness of a winner (player wins
		// as fast as possible). This returns both the fitness of the winner and the loser.
		selfFit = totalPieces / 2 + totalPieces % 2 - connectNum + 1;
		//if it's a tie
		if (isGameOver(lastPlayer) == 0) {
			selfFit++;
			otherFit = selfFit;
		}
		//loser score
		else
			otherFit = maxPieces - selfFit - connectNum + 1;
		
		int[] fitnesses = {selfFit, otherFit};
		return fitnesses;
	}
	
	/**
	 * Calculates the fitnesses for a game of Connect N with any number of players.
	 * 
	 * @param winner
	 *            the player that won. A value less than 1 indicates a tie.
	 * @return the fitnesses of all of the participants in the game.
	 */
	public double[] calcFitnesses(int winner) {
		winner--; //Convert player number to array number
		double[] fitness = new double[pieces.length];
		//If it is a tie, all of the players get the loser fitness (how long they lasted) 
		for (int i = 0; i < fitness.length; i++) {
			if (i == winner)
				fitness[i] = pieces[i] - connectNum + 1;
			else
				fitness[i] = ((double) maxPieces) / ((double) pieces.length) - pieces[i];
		}
		return fitness;
	}
}
