package gpjpp;

/**
 * A helper interface for the generalized get function in GPProperties.
 * @author Joshua Lipstone
 *
 */
@FunctionalInterface
public interface PropertyConverter<T, R> {
	public R convert(T property, Object def);
}