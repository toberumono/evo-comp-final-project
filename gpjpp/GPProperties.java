// gpjpp (genetic programming package for Java)
// Copyright (c) 1997, Kim Kokkonen
//
// This program is free software; you can redistribute it and/or 
// modify it under the terms of version 2 of the GNU General Public 
// License as published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// Send comments, suggestions, problems to kimk@turbopower.com

package gpjpp;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Extends the standard Properties class. GPProperties fixes mishandling of tab and space in the load() method of JDK 1.0
 * (fixed in JDK 1.1). It makes keynames case-insensitive ala Windows ini files. And it allows comments at the end of each
 * line, delimited by the first space, tab, #, or ! character after the value string. This means that the value string cannot
 * contain any of these characters, of course, but this does not impose an important limitation for this package.
 *
 * @version 2.0
 * @author Joshua Lipstone (v2.0)
 */
public class GPProperties extends HashMap<String, String> {
	private static final PropertyConverter<String, Integer> toInt = (s, d) -> {
		try {
			return Integer.parseInt(s);
		}
		catch (NumberFormatException e) {
			return (Integer) d;
		}
	};
	private static final PropertyConverter<String, Double> toDouble = (s, d) -> {
		try {
			return Double.parseDouble(s);
		}
		catch (NumberFormatException e) {
			return (Double) d;
		}
	};
	private static final PropertyConverter<String, Boolean> toBoolean = (s, d) -> {
		try {
			return Integer.parseInt(s) != 0;
		}
		catch (NumberFormatException e) {
			return Boolean.valueOf(s);
		}
	};
	private static final HashMap<Class<?>, PropertyConverter<String, ?>> converters;
	static {
		converters = new HashMap<>();
		addPropertyConverter(Integer.class, toInt);
		addPropertyConverter(int.class, toInt);
		addPropertyConverter(Double.class, toDouble);
		addPropertyConverter(double.class, toDouble);
		addPropertyConverter(Boolean.class, toBoolean);
		addPropertyConverter(boolean.class, toBoolean);
		addPropertyConverter(String.class, (s, d) -> (s == null ? (String) d : s));
		addPropertyConverter(String[].class, (s, d) -> (s == null ? (String[]) d : s.split(", ?")));
	}
	
	/**
	 * Creates an empty property set.
	 */
	public GPProperties() {
		super();
	}
	
	/**
	 * Creates an empty property set with specified default values.
	 */
	public GPProperties(Map<String, String> defaults) {
		super(defaults);
	}
	
	public GPProperties(Path file) throws IOException {
		super();
		load(file);
	}
	
	/**
	 * Looks up a property value based on a key. The key is forced to lowercase before calling Properties.getProperty.
	 */
	public String get(String key) {
		return super.get(key.toLowerCase());
	}
	
	/**
	 * Looks up a property value based on a key. The key is forced to lowercase before accessing the map.<br>
	 * If the property is found, it gets converted to the type specified by {@code type}.<br>
	 * If the property cannot be found, it returns def.
	 */
	public <T> T get(String key, Class<T> type, T def) {
		@SuppressWarnings({"unchecked"})
		T out = (T) converters.get(type).convert(get(key), def);
		return out;
	}
	
	/**
	 * Looks up an enumerated property value based on a key. The key is forced to lowercase before accessing the map.<br>
	 * If the property is found, its index in {@code enumerated} is returned.<br>
	 * If the property cannot be found or is not in {@code enumerated}, it returns def.
	 * 
	 * @see #getEnumeratedType
	 */
	public Integer get(String key, String[] enumerated, Integer def) {
		return getEnumeratedType(key, enumerated, def);
	}
	
	/**
	 * Loads properties from the file at p. A property consists of a key, and equals sign, and a value. The value cannot
	 * contain spaces. Any space, tab, !, or # after the start of the value component marks the beginning of a comment.
	 */
	public synchronized void load(Path p) throws IOException {
		final Pattern prop = Pattern.compile("(.+?) *= *(.+?)#.*");//Pattern.compile("(.+?)[ \t]*=[ \t]*([^ \t!#]+).*");
		Files.lines(p).forEach(s -> {
			Matcher m = prop.matcher(s);
			if (m.find())
				put(m.group(1).trim().toLowerCase(), m.group(2).trim().toLowerCase());
		});
	}
	
	/**
	 * Writes the contents of the properties to System.out. For testing and debugging.
	 */
	public void show() {
		forEach((k, v) -> {
			System.out.println(k + "=" + v);
		});
	}
	
	public int getInt(String property, int def) {
		return get(property, int.class, def);
	}
	
	public double getDouble(String property, double def) {
		return get(property, double.class, def);
	}
	
	public boolean getBoolean(String property, boolean def) {
		return get(property, boolean.class, def);
	}
	
	public <T> T getProperty(String property, PropertyConverter<String, T> converter, T def) {
		return converter.convert(get(property), def);
	}
	
	public int getEnumeratedType(String property, String[] enumerated, int def) {
		String s = get(property);
		if (s == null)
			return def;
		try {
			//use integer value if specified
			return Integer.parseInt(s);
		}
		catch (NumberFormatException e) {
			//check string values
			for (int i = 0; i < enumerated.length; i++)
				if (s.equalsIgnoreCase(enumerated[i]))
					return i;
			return def;
		}
	}
	
	/**
	 * Used to add a {@link PropertyConverter} to the converters map. The converter must have the same return type as the
	 * type it is being mapped to.
	 */
	public static <T> PropertyConverter<String, ?> addPropertyConverter(Class<T> type, PropertyConverter<String, T> converter) {
		return converters.put(type, converter);
	}
}
