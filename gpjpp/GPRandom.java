// gpjpp (genetic programming package for Java)
// Copyright (c) 1997, Kim Kokkonen
//
// This program is free software; you can redistribute it and/or 
// modify it under the terms of version 2 of the GNU General Public 
// License as published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// Send comments, suggestions, problems to kimk@turbopower.com

package gpjpp;

import java.util.Random;

/**
 * Extends the standard {@link java.util.Random Random} class.
 * This adds a couple of random number methods that are of use to gpjpp.
 * A static instance of GPRandom can be found in {@link GP GP}.
 * @version 2.0
 */
public class GPRandom extends Random {

	public GPRandom() { super(); }

	/**
	 * Returns true if a random real in the range from 0.0 to 100.0
	 * is less than the specified percent. Thus, if percent is 50.0,
	 * the result has equal probability of returning true or false.
	 * @param percent the non-decimal form percent chance of returning true
	 * @return the result of a coin flip with the given percentage weights
	 */
	public boolean flip(double percent) {
		return (nextDouble()*100.0 < percent)? true : false;
	}
}
